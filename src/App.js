import React from 'react'
import { Route, Switch } from 'react-router-dom'
import routes from './router'
function App() {
  	return (
		<Switch>
			{ routes.map((route, idx) => (
				<Route 
					exact path={route.path} 
					component={route.component} 
					key={idx}>
				</Route>
			))}
		</Switch>
  	);
}

export default App;
