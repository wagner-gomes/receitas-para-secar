import React, { Component } from 'react';

// Css globals
import '@assets/sass/pages/home/bootstrap.scss'
import '@assets/sass/pages/home/home.scss'

// Components
import { 
    Banner, 
    FaixaComprar,
    VideoVenda,
    Passo, 
    Receitas, 
    PeriodoMudancas,
    ClientesSlider,
    PerguntasFreq,
    Garantia,
    Copyright 
} from '@components';

class Home01 extends Component {
    constructor(){
        super();
        this.state = {
            linkAfiliate: 'https://go.hotmart.com/C45173452O?ap=7273'
        }
    }
    
    render() {
        return (
            <section className="container-site">               
                <Banner />
                <Passo />
                <VideoVenda linkcomprar={this.state.linkAfiliate}/>
                <Receitas />
                <FaixaComprar linkcomprar={this.state.linkAfiliate}/>
                <PeriodoMudancas />
                <Garantia />
                <ClientesSlider corbg="#ffeff2" /> 
                <PerguntasFreq />
                <FaixaComprar linkcomprar={this.state.linkAfiliate}/>
                <Copyright />
            </section>
        );
    }
}

export default Home01;
