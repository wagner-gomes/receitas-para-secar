import React from 'react'

import '@styles/video-venda.scss'

const VideoVenda = ({ linkcomprar }) => {
    return (          
        <section className="overview-area ptb-100">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-md-6 col-sm-12">
                            <div className="overview-video">
                                <a href="https://www.youtube.com/watch?v=pR8lBftx8kU" className="video-btn popup-youtube">
                                    <i className='bx bxl-youtube'></i>
                                </a>
                            </div> 
                        </div>
                        <div className="col-md-6 col-sm-12">
                            <div className="overview-content">
                                <h3>Veja como é simples se livrar naturalmente de 5 a 10kg em apenas 30 dias!</h3>
                                <p>Sem ter que passar fome e sem dietas malucas (ASSISTA O VÍDEO!)</p>
            
                                <div className="overview-btn">
                                    <a target="_blank" href={linkcomprar} className="default-btn">Sim, eu quero minhas receitas!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    )
}

export default VideoVenda;
