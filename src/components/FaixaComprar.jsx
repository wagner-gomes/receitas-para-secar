import React from 'react'

import '@styles/faixa-comprar.scss'

const FaixaComprar = ({ linkcomprar }) => {
    console.log(linkcomprar);

    const listArray = [
        { id: 1, srcimg: './static/images/logos-pagto/01.png', altimg: '' },
        { id: 2, srcimg: './static/images/logos-pagto/02.png', altimg: '' },
        { id: 3, srcimg: './static/images/logos-pagto/03.png', altimg: '' },
        { id: 4, srcimg: './static/images/logos-pagto/04.png', altimg: '' },
        { id: 5, srcimg: './static/images/logos-pagto/05.png', altimg: '' },
        { id: 6, srcimg: './static/images/logos-pagto/06.png', altimg: '' },
        { id: 7, srcimg: './static/images/logos-pagto/07.png', altimg: '' },
        { id: 8, srcimg: './static/images/logos-pagto/08.png', altimg: '' },
    ];

    return (
        <section data-bg={ linkcomprar } className="faixa-comprar pt-4 pb-4">
            <div className ="container">
                <div className="row align-items-center">
                    <div className="col-12">
                        <div className="fc-content d-flex flex-column align-items-center justify-content-center">
                            <div className="fc-btn">
                            <a target="_blank" href={linkcomprar}  className="default-btn">Sim, eu quero minhas receitas!</a>
                            </div>
                            <ul className="fc-logos">
                                { 
                                    listArray.map((e, i) => {
                                        return (<li href={linkcomprar}  key={i} className="item" style={{ backgroundImage: 'url('+e.srcimg+')' }}></li>)
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default FaixaComprar;
