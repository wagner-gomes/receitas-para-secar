import React from 'react';

// Css
import '@styles/receitas.scss'

const Receitas = () => {
    const cardArray = [
        {
            id: 1,
            classimg: 'featured-post mg-b43',
            srcimg: './static/images/receitas/sucos-detox-para-desintoxicar.png',
            altimg: 'sucos-detox-para-desintoxicar',
            title: 'Sucos Detox Para Desintoxicar',
            text01: "Os estudos apontaram que além de reduzir o colesterol o detox também elimina gordura pelas fezes e urina.",
        },{
            id: 2,
            classimg: 'featured-post mg-b42',
            srcimg: './static/images/receitas/sopas-milagrosas.png',
            altimg: 'images',
            title: 'Sopas Milagrosas',
            text01: "A combinação perfeita para queimar gordura em uma sopa super gostosa que substitui refeições.",
        },{
            id: 3,
            classimg: 'featured-post mg-b40',
            srcimg: './static/images/receitas/shakes-funcionais.png',
            altimg: 'images',
            title: 'Shakes Funcionais',
            text01: "Ótima bebida com bastante nutrientes para aumentar sua imunidade e saciar suas fome de preparo rápido.",
        },{
            id: 4,
            classimg: 'featured-post mg-b43',
            srcimg: './static/images/receitas/o-cha-mais-poderoso-do-brasil.png',
            altimg: 'images',
            title: 'O chá mais poderoso do Brasil',
            text01: "Receita secreta de um dos chás mais procurados no Brasil quando o assunto é perca de peso. Incríveis os Resultados.",
        },{
            id: 5,
            classimg: 'featured-post mg-b42',
            srcimg: './static/images/receitas/bebidas-bomba.png',
            altimg: 'images',
            title: 'Bebidas Bomba',
            text01: "Muitos problemas cardíacos são causadas por problemas circulatórios. O yoga ajuda a bombear o sangue e a circular o oxigênio para todas as partes do corpo de forma saudável.",
        },{
            id: 6,
            classimg: 'featured-post mg-b40',
            srcimg: './static/images/receitas/cardapios-fit.png',
            altimg: 'images',
            title: 'Cardápios Fit',
            text01: "Cardápios práticos para a semana toda. Lembrando que não é uma dieta e sim uma reeducação. A nossa intenção é que você coma saudável.",
        }

    ]

    return (       
        <section className="receitas-area pt-50 pb-50">
            <div className="container">
                <div className="section-title">
                    <p className="mb-md-5">
                        Atenção: Se você é uma pessoa que precisa muito ter resultados e que sofre 
                        frequentemente com ANSIEDADE,  o nosso PROGRAMA pode, definitivamente, te 
                        ajudar a partir de agora. E o melhor de tudo, é só R$37,0
                    </p>
                    <h2>O Programa é a base de que alimentos simples que você tem em casa, são muitas receitas como:</h2>
                </div>
                <div className="row">
                    { cardArray.map((e, i) => {
                        return (
                            <div key={ i } className="col-lg-4 col-sm-6">
                                <div className="single-receitas">
                                    <div className="receitas-image">
                                        <div className="box-image">
                                            <img src={ e.srcimg } alt={ e.altimg }/>
                                        </div>
                                        
                                        <div className="content">
                                            <h3><a href="#">{ e.title }</a></h3>
                                            <p>{ e.text01 }</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )})
                    }
                </div>
            </div>
        </section>
    )
}

export default Receitas;
