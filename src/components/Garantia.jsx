import React from 'react'

import '@styles/garantia.scss'

const Garantia = () =>(          
    <section style={{ backgroundColor: '#ccc' }} className="garantia-area ptb-100">
            <div className="container">
                <div className="row align-items-center mb-3">
                    <div className="col-12 text-center">
                        <h2>VOCÊ TEM AINDA 7 DIAS DE GARANTIA INCONDICIONAL!</h2>
                        <p className="text-bold">VOCÊ COSTUMA PERDER OPORTUNIDADES COMO ESSAS?</p>
                    </div>
                </div>
                <div className="row align-items-center">
                    <div className="col-md-4">
                        <div className="g-left text-center text-md-right">
                            <p className="pl-md-5"> 
                                Tem alguma dúvida de que as Receitas Para Secar irá funcionar pra você?
                                Sem problemas, tire proveito da nossa garantia incondicional de 7 dias e teste por você mesma (o).
                            </p>
                        </div>
                    </div>
                    <div className="col-md-2">
                        <div className="g-main">
                           <img className="g-img-selo" src="./static/images/selo-de-qualidade.png" alt="selo-de-qualidade"/>
                        </div>
                    </div>
                    <div className="col-md-5">
                        <div className="g-right text-center text-md-left">
                            <p>Valor do investimento para emagrecer? <br/> de <strike style={{ color: '#fdd50d' }}> R$67,00</strike> <br/>por apenas 4x de R$9,83 <br/>ou <span style={{ color: '#fdd50d' }}>R$37,00</span> à vista</p>
                        </div>
                    </div>
                </div>
                <div className="row align-items-center mt-3">
                    <div className="col-12 text-center">
                        <p className="text-bold">SEU EMAGRECIMENTO GARANTIDO OU SEU DINHEIRO DE VOLTA!</p>
                    </div>
                </div>
            </div>
    </section>
)
    
export default Garantia;
