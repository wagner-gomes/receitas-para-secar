import Banner           from './Banner'
import Passo            from './PassoAPasso'
import Receitas         from './Receitas'
import PeriodoMudancas  from './PeriodoMudancas'
import ClientesSlider   from './ClientesSlider'
import PerguntasFreq    from './PerguntasFreq'
import VideoVenda       from './VideoVenda'
import Garantia         from './Garantia'
import FaixaComprar     from './FaixaComprar'
import Copyright        from './Copyright'

export { 
    Banner, 
    VideoVenda,
    FaixaComprar,
    Passo, 
    Receitas, 
    PeriodoMudancas,
    ClientesSlider, 
    PerguntasFreq,
    Garantia,
    Copyright 
}