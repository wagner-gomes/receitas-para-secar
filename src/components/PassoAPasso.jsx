import React from 'react';
import { HiOutlineBookOpen } from 'react-icons/hi';
import { BiAlarmExclamation, BiCalendar } from "react-icons/bi";
import { RiCupLine } from "react-icons/ri";
import { FiFacebook } from "react-icons/fi";
import { AiOutlineApple } from "react-icons/ai";

// Css
import '@styles/passo-a-passo.scss'

const PassoAPasso = () => {
    const cardArray = [
        {
            id: 1,
            classimg: 'featured-post mg-b43',
            srcimg: './static/images/passo-a-passo/pencil.png',
            altimg: 'images',
            icon: <HiOutlineBookOpen />,
            title: 'PASSO 1',
            text01: "Ler todo o seu material atentamente antes de iniciar",
        },{
            id: 2,
            classimg: 'featured-post mg-b42',
            srcimg: './static/images/passo-a-passo/clock.png',
            altimg: 'images',
            icon: <BiAlarmExclamation />,
            title: 'PASSO 2',
            text01: "Programar seu tempo para cada receita e comer corretamente nos horários",
        },{
            id: 3,
            classimg: 'featured-post mg-b40',
            srcimg: './static/images/passo-a-passo/tea.png',
            altimg: 'images',
            icon: <RiCupLine />,
            title: 'PASSO 3',
            text01: "Tomar todos os dias os chás, os detox e a bebida bomba do ebook",
        },{
            id: 4,
            classimg: 'featured-post mg-b43',
            srcimg: './static/images/passo-a-passo/calendar.png',
            altimg: 'images',
            icon: <BiCalendar />,
            title: 'PASSO 4',
            text01: "Seguir o cronograma dos 30 dias corretamente, se pese ao iniciar para calcular os resultados no fim do desafio",
        },{
            id: 5,
            classimg: 'featured-post mg-b42',
            srcimg: './static/images/passo-a-passo/facebook.png',
            altimg: 'images',
            icon: <FiFacebook />,
            title: 'PASSO 5',
            text01: "Participar do grupo vip do facebook com todas as 15 mil participantes. O grupo vai te manter no foco",
        },{
            id: 6,
            classimg: 'featured-post mg-b40',
            srcimg: './static/images/passo-a-passo/harvest.png',
            altimg: 'images',
            icon: <AiOutlineApple />, 
            title: 'PASSO 6',
            text01: "Organizar os ingredientes da semana de suas receitas",
        }

    ]

    return (       
        <section className="department-area pt-50 pb-50">
            <div className="container">
                <div className="section-title">
                    <h2>Incrível método, basta você seguir esse passo a passo <span style={{color: 'rgb(50, 185, 91)'}}>para conseguir a começar ter resultados:</span></h2>
                    <p>(qualquer pessoa consegue emagrecer assim)</p>
                </div>
                <div className="row">
                    { cardArray.map((e, i) => {
                        return (
                            <div key={ i } className="col-lg-4 col-sm-6">
                                <div className="single-department">
                                    <div className="department-image">
                                        {/* <i className="icons"> { e.icon } </i> */}
                                        <div style={{ backgroundImage:  `url(${e.srcimg})` }} className="iconbg"></div>
                                        
                                        <div className="content">
                                            <h3><a href="#">{ e.title }</a></h3>
                                            <p>{ e.text01 }</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )})
                    }
                </div>
                <p style={{color: 'rgb(11, 146, 21)', fontSize: '18px', lineHeight: '23px', textAlign: 'center'}}>Ajudamos mais de 15.884 pessoas no Brasil e mais 7 países, homens e mulheres de todas as idades que decidiram emagrecer conosco neste ano</p>
            </div>
        </section>
    )
}

export default PassoAPasso;
