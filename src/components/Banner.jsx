import React from 'react'

import '@styles/main-banner.scss'

const Banner = () =>(          
    <div className="main-banner">
        <div className="main-banner-item">
            <div className="d-table">
                <div className="d-table-cell">
                    <div className="container">
                        <div className="main-banner-content">
                            <h1>
                                As receitas mais poderosas do Brasil para perde de peso com saúde
                            </h1>
                            <span>Aprenda a emagrecer agora!</span>
                            <div className="banner-btn d-none">
                                <a href="shop-1.html" className="default-btn">Our Shop</a>
                                <a href="cart.html" className="optional-btn">Add to Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
)
    
export default Banner;
