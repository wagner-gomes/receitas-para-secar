import React from 'react'

import '@styles/clientes-slider.scss'

const ClientesSlider = ({ corbg }) => {

    corbg  = corbg ? corbg : '#eaeaef';

    const cardArray = [
        {
            id: 1,
            srcimg: './static/images/rede-social/rede-social-1.jpg',
            altimg: 'images',
        },{
            id: 2,
            srcimg: './static/images/rede-social/rede-social-2.jpg',
            altimg: 'images',
        },{
            id: 3,
            srcimg: './static/images/rede-social/rede-social-3.jpg',
            altimg: 'images',
        },{
            id: 4,
            srcimg: './static/images/rede-social/rede-social-4.jpg',
            altimg: 'images',
        },{
            id: 5,
            srcimg: './static/images/rede-social/rede-social-5.jpg',
            altimg: 'images'
        },{
            id: 6,
            srcimg: './static/images/rede-social/rede-social-6.jpg',
            altimg: 'images'
        }

    ]

    return (          
        <section data-bg={ corbg }  className="top-products-area pt-50 pb-50">
            <div className="container-fluid">
                <div className="section-title">
                    <h2>Vejam o que estão falando sobre o nosso método</h2>
                </div>
                <div className="top-products-slider owl-carousel owl-theme">
                    { cardArray.map((e, i) => {
                        return (
                            <div key={ i } className="top-products-item">
                                <div className="products-image">
                                    <img src={ e.srcimg } alt="image"/>
                                </div>
                            </div>
                        )})
                    }
                </div>
            </div>
        </section>
    )
}
    
export default ClientesSlider;
