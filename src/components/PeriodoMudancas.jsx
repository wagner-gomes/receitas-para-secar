import React from 'react';
// import { AiOutlineCheckCircle } from "react-icons/ai";

// Css
import '@styles/periodo-mudancas.scss'

const PeriodoMudancas = () => {
    const cardArray = [
        {
            id: 1,
            title: 'Sua auto-estima vai mudar, vai se sentir mais bonita por estar mais magra',
        },{
            id: 2,
            title: 'Vai perder a vergonha de mostrar barriga na hora de usar um biquíni ou uma roupa mais ousada',
        },{
            id: 3,
            title: 'Acelerar seu metabolismo e queimar mais calorias muito mais rápido',
        },{
            id: 4,
            title: 'Eliminar o inchaço naturalmente pelas fezes e urina',
        },{
            id: 5,
            title: 'Melhorar o seu sistema digestivo, você não vai passar mais dias e dias sem ir ao banheiro',
        },{
            id: 6,
            title: 'Reduzir de 5 a 25cm na região da barriga, vamos triturar a gordura abdominal',
        },{
            id: 7,
            title: 'Ter uma pele mais limpa e macia sem estrias e celutites',
        }

    ]

    return (       
        <section className="periodo-mudancas pt-50 pb-70">
            <div className="container">
                <div className="section-title">
                    <h2>Durante esse período você vai passar por essas mudanças:</h2>
                </div>
                <div className="row">
                    { cardArray.map((e, i) => {
                        return (
                            <div key={ i } className="col-lg-4 col-sm-6 d-flex mb-4">
                                <div className="card">
                                    <div className="card-body">
                                        <div className="card-text">
                                            {/* <i style={{ fontSize: "20px"}}><AiOutlineCheckCircle /></i>  */}
                                           <p> { e.title }</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )})
                    }
                </div>
            </div>
        </section>
    )
}

export default PeriodoMudancas;
