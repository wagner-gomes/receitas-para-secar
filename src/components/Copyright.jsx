import React from 'react'

import '@styles/copyright-area.scss'

const Copyright = () =>(          
    <div className="copyright-area">
        <div className="container">
            <div className="copyright-area-content">
                <div className="row align-items-center justify-content-center">
                    <div className="col-lg-6 col-md-6 text-center">
                        <p className="text-copyright">
                            Copyright 2020  <i className="fa fa-copyright"></i>  Todos os direitos reservados
                        </p>
                    </div>

                    <div className="col-lg-6 col-md-6 d-none">
                        <ul>
                            <li> <a href="terms-of-service.html">Terms & Conditions</a> </li>
                            <li> <a href="privacy-policy.html">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
)
    
export default Copyright;
