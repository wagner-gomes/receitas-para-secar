import React from 'react'

import '@styles/perguntas-freq.scss'

const PerguntasFreq = () => {
    // corbg  = corbg ? corbg : '#eaeaef';

    const listArray = [
        {
            id: 1,
            perg: 'O site é seguro?',
            resp: 'Totalmente seguro e protegido! O site que gerencia nossos pagamentos é 100% confiável contra golpes.',
        },{
            id: 2,
            perg: 'Quanto tempo de acesso ao material eu terei?',
            resp: 'Após a compra ser confirmada o e-book é seu para sempre, para isso basta você guardar direitinho e acessar quando e onde quiser.',
        },{
            id: 3,
            perg: 'Como irei acessar o e-book?',
            resp: 'O acesso ao e-book é logo após a confirmação do pagamento. Após a confirmação, é enviado o acesso no seu e-mail e você pode acessar quando quiser pelo computador, notebook, celular ou tablet.',
        },{
            id: 4,
            perg: 'Quais as formas de pagamento?',
            resp: 'Pagamentos no débito/transferência você recebe seu material em algumas horas em seu email, pagamentos no Crédito você recebe seu material na hora em seu email, pagamentos no boleto você recebe em até 3 dias úteis em seu email e pagamentos pelo Paypal recebe em minutos.',
        },{
            id: 5,
            perg: 'Como faço para comprar o e-book?',
            resp: 'Para realizar sua compra basta clicar em qualquer botão desse site e ir direto para a página de pagamento, lá, é só preencher os dados e escolher a forma de pagamento que desejar.',
        },{
            id: 6,
            perg: 'Quais os maiores benefícios que o programa irá me proporcionar?',
            resp: 'Mais qualidade de vida, mais disposição, grande melhora na pele, intestino regulado e perca de peso de forma saudável.',
        },{
            id: 7,
            perg: 'Em quanto tempo eu posso começar a ver resultados?',
            resp: 'Os resultados vem logo nas primeiras semanas se você seguir o nosso passo a passo. Você vai perceber que suas roupas vão começar a ficar mais folgadas e você vai desinchar muito.',
        },{
            id: 8,
            perg: 'Meu dia é corrido e não tenho muito tempo. O programa vai me ajudar?',
            resp: 'Sim. Temos várias clientes que também trabalham o dia inteiro e ainda sim conseguem ter resultados excelentes.',
        },{
            id: 9,
            perg: 'A partir do momento que eu compro, em quanto tempo eu recebo o conteúdo?',
            resp: 'Você vai receber o seu material imediatamente após a confirmação de sua compra.',
        },{
            id: 10,
            perg: 'Eu nunca fiz uma reeducação alimentar antes, será que eu consigo?',
            resp: 'Definitivamente esse é o programa mais indicado para você, pois ele é a forma mais simplificada para ter resultados com saúde. Você vai amar participar e seguir o nosso programa',
        }
    ]

    return (          
        <section className="faq-area pt-50 pb-50">
            <div className="container">
                <div className="section-title">
                    <h2>Dúvidas Frequentes</h2>
                </div>

                <div className="faq-accordion">
                    <ul className="accordion">
                        { listArray.map( (e, i) => {
                            return(
                                <li key={ i } className="accordion-item">
                                    <a className="accordion-title "><i className='bx bx-plus'></i>{ e.perg }</a>
                                    <p className="accordion-content">{ e.resp }</p>
                                </li>
                            )
                        })}
                    </ul>
                </div>
            </div>
        </section>
    )
}
    
export default PerguntasFreq;
