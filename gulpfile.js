'use strict';

/**
 * Abaixo com a versão Gulp 4
 * Referencias:
 * https://gist.github.com/atelic/8eb577e87a477a0fb411
 */

var gulp             = require('gulp');
// var gulpJshint    = require('gulp-jshint');
// var gulpBabel        = require('gulp-babel');
// var gulpPlumber      = require('gulp-plumber')
// var gulpClean        = require('gulp-clean');
// var gulpConcat       = require('gulp-concat');
// var gulpUglify       = require('gulp-uglify');
// var eventStream      = require('event-stream');
// var htmlmin        = require('gulp-htmlmin');
// var cleanCSS         = require('gulp-clean-css');
// var runSequence   = require('gulp4-run-sequence');
// var gulpCount        = require('gulp-count');
// var gulpRename       = require('gulp-rename');
var gulpSass         = require('gulp-sass');
// var pipeline      = require('readable-stream').pipeline;
// var copy          = require('copy').pipeline;
// var gulpCssMin       = require('gulp-cssmin');
// var gulpMinifyCss    = require('gulp-minify-css');
var gulpAutoprefixer = require('gulp-autoprefixer');
var gulpSourcemaps   = require('gulp-sourcemaps');
var gulpMerge        = require('merge-stream');
// var gulpWebp         = require('gulp-webp');


/**
 * Copy sass Bootstrap e mdbootstrap
 */
gulp.task('copy-sass-bs', gulp.series( function () {
    var firstPath =  gulp.src(
        [ 'node_modules/bootstrap/scss/**/*'], { allowEmpty: true }  
    )
    .pipe(gulp.dest('src/assets/sass/elements/bootstrap'));

    return gulpMerge(firstPath);
 }));


// Sass
gulp.task('sass', gulp.series(function() {
    return gulp.src(['src/assets/sass/*.scss', 'src/assets/sass/pages/**/*.scss' ])
        .pipe( gulpSass().on('error', gulpSass.logError) )
        .pipe( gulpSass({outputStyle: 'compressed'}).on('error', gulpSass.logError))
        .pipe( gulpAutoprefixer(
            {
                overrideBrowserslist: ['last 2 versions'],
                cascade: false
            }
        ))
        .pipe(gulp.dest('src/assets/css/pages'))
}));



// Default
gulp.task('default', gulp.series( function() {
    gulp.watch([ 'src/assets/sass/**/*.scss', 'src/assets/sass/pages/**/*.scss' ], gulp.parallel(['sass']  ));
}));
