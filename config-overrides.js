/** 
 * customize-cra
 * Doc https://github.com/arackaf/customize-cra/blob/master/api.md
 * 
 * https://stackoverflow.com/questions/58881130/how-to-add-webpack-plugin-to-react-rewired
 * https://dev.to/nodewarrior/override-cra-and-add-webpack-config-without-ejecting-2f3n
 * https://npm.runkit.com/customize-cra-my
 */ 

var webpack = require('webpack');
const rewireProvidePlugin = require('react-app-rewire-provide-plugin');

const {
    override,
    addDecoratorsLegacy,
    disableEsLint,
    addBundleVisualizer,
    addWebpackAlias,
    adjustWorkbox,
    addLessLoader,
    addPostcssPlugins,
    adjustStyleLoaders,
} = require("customize-cra");

const path = require("path");
// const postcss = require("postcss-loader");
// const css = require("css-loader"); 
// const resolve = require("resolve-url-loader");
// const processor = require("sass-loader");

module.exports = override(
    // enable legacy decorators babel plugin
    addDecoratorsLegacy(),

    /**  disable eslint in webpack */
    // disableEsLint(),

    /** Adds the bundle visualizer  */
    addBundleVisualizer({}, true),

    // add webpack bundle visualizer if BUNDLE_VISUALIZE flag is enabled
    // process.env.BUNDLE_VISUALIZE == 1 && addBundleVisualizer(),

    /** add an alias for */
    addWebpackAlias({
        ['@assets']: path.resolve(__dirname, './src/assets'),
        ['@styles']: path.resolve(__dirname, './src/assets/sass/components/home01'),
        ['@components']: path.resolve(__dirname, './src/components'),
        ['@layouts']: path.resolve(__dirname, './src/layouts'),
        ['@store']: path.resolve(__dirname, './src/store'),
        ['@pages']: path.resolve(__dirname, './src/pages'),
        ['@helpers']: path.resolve(__dirname, './src/helpers'),
    }),

);

